
/label ~change ~"change::unscheduled" ~"group::global search" ~"Service::Search" ~C3

<!--
Please review https://about.gitlab.com/handbook/engineering/infrastructure/change-management/ for the most recent information on our change plans and execution policies.
-->

# Production Change

### Change Summary

This change will remove migrations in the `gitlab-production-migrations` index that do not exist in the codebase. The migration files were either reverted or refactored and exist under different documents

List out each migration:

* `VERSION` (`NAME`) - List reason for removal/link to MR
* `VERSION` (`NAME`) - List reason for removal/link to MR
* `VERSION` (`NAME`) - List reason for removal/link to MR

### Change Details

<!--
To automatically add your change to the GitLab Production calendar update the following fields:
- Time tracking
- Scheduled Date and Time (UTC in format YYYY-MM-DD HH:MM)

Bot: https://gitlab.com/gitlab-com/gl-infra/ops-team/toolkit/change-scheduler
-->

1. **Services Impacted**  - <!-- woodhouse: '{{ .ServiceLabels }}' -->{+ ~"Service::Search" +}
1. **Change Technician**  - <!-- woodhouse: '`@{{ .Username }}`' -->{+ DRI for the execution of this change +}
1. **Change Reviewer**    - <!-- woodhouse: '@{{ .Reviewer }}' -->{+ DRI for the review of this change +}
1. **Scheduled Date and Time (UTC in format YYYY-MM-DD HH:MM)** - {+ Start date and time planned to execute change steps YYYY-MM-DD HH:MM +}
1. **Time tracking**          - <!-- woodhouse: '{{ .Duration}}' -->{+ Time, in minutes, hours, or days, needed to execute all change steps, including rollback +}
1. **Downtime Component** - <!-- woodhouse: '{{ .Downtime }}' -->{+ 0 minutes +}

## Detailed steps for the change

### Change Steps - steps to take to execute the change

*Estimated Time to Complete (mins)* - 2 minutes

Update with all delete commands to be run

```
DELETE /gitlab-production-migrations/_doc/VERSION
DELETE /gitlab-production-migrations/_doc/VERSION
DELETE /gitlab-production-migrations/_doc/VERSION
```

- [ ] Set label ~"change::in-progress" `/label ~change::in-progress`
- [ ] Post an internal comment with the content for each document in the issue for **Staging**
- [ ] Delete each document on [**Staging** - Dev Tools](https://com-gitlab-staging-indexing-20220519.kb.us-central1.gcp.cloud.es.io:9243/app/dev_tools#/console)
- [ ] Post an internal comment with the content for each document in the issue for **Production**
- [ ] Delete each document on [**Production** - Dev Tools](https://gprd-indexing-20220523.kb.us-central1.gcp.cloud.es.io:9243/app/dev_tools#/console)
- [ ] Set label ~"change::complete" `/label ~change::complete`

## Rollback

### Rollback steps - steps to be taken in the event of a need to rollback this change

*Estimated Time to Complete (mins)* - {+Estimated Time to Complete in Minutes+}

- [ ] Re-insert the documents into the index
- [ ] Set label ~"change::aborted" `/label ~change::aborted`

## Monitoring

### Key metrics to observe

<!--
  * Describe which dashboards and which specific metrics we should be monitoring related to this change using the format below.
-->

- Metric: Sidekiq logs
  - Location: https://log.gprd.gitlab.net/app/kibana#/discover?_g=()&_a=(columns:!(_source),index:AW5F1e45qthdGjPJueGO,interval:auto,query:(language:kuery,query:''),sort:!(!(json.time,desc)))
  - What changes to this metric should prompt a rollback: Errors when checking the migrations index

## Change Reviewer checklist

<!--
To be filled out by the reviewer.
-->
~C4 ~C3 ~C2 ~C1:
- [ ] Check if the following applies:
  - The **scheduled day and time** of execution of the change is appropriate.
  - The [change plan](#detailed-steps-for-the-change) is technically accurate.
  - The change plan includes **estimated timing values** based on previous testing.
  - The change plan includes a viable [rollback plan](#rollback).
  - The specified [metrics/monitoring dashboards](#key-metrics-to-observe) provide sufficient visibility for the change.

~C2 ~C1:
- [ ] Check if the following applies:
  - The complexity of the plan is appropriate for the corresponding risk of the change. (i.e. the plan contains clear details).
  - The change plan includes success measures for all steps/milestones during the execution.
  - The change adequately minimizes risk within the environment/service.
  - The performance implications of executing the change are well-understood and documented.
  - The specified metrics/monitoring dashboards provide sufficient visibility for the change.
      - If not, is it possible (or necessary) to make changes to observability platforms for added visibility?  
  - The change has a primary and secondary SRE with knowledge of the details available during the change window.
  - The change window has been agreed with Release Managers in advance of the change. If the change is planned for APAC hours, this issue has an agreed pre-change approval. 
  - The labels ~"blocks deployments" and/or ~"blocks feature-flags" are applied as necessary.

## Change Technician checklist

<!--
To find out who is on-call, in #production channel run: /chatops run oncall production.
-->

- [ ] Check if all items below are complete:
  - The [change plan](#detailed-steps-for-the-change) is technically accurate.
  - This Change Issue is linked to the appropriate Issue and/or Epic
  - Change has been tested in staging and results noted in a comment on this issue.
  - A dry-run has been conducted and results noted in a comment on this issue.
  - The change execution window respects the [Production Change Lock periods](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/#production-change-lock-pcl).
  - For ~C1 and ~C2 change issues, the change event is added to the [GitLab Production](https://calendar.google.com/calendar/embed?src=gitlab.com_si2ach70eb1j65cnu040m3alq0%40group.calendar.google.com) calendar.
  - For ~C1 and ~C2 change issues, the SRE on-call has been informed prior to change being rolled out. (In #production channel, mention `@sre-oncall` and this issue and await their acknowledgement.)
  - For ~C1 and ~C2 change issues, the SRE on-call provided approval with the ~eoc_approved label on the issue.
  - For ~C1 and ~C2 change issues, the Infrastructure Manager provided approval with the ~manager_approved label on the issue.
  - Release managers have been informed prior to any C1, C2, or ~"blocks deployments" change being rolled out. (In #production channel, mention `@release-managers` and this issue and await their acknowledgment.) 
  - There are currently no [active incidents](https://gitlab.com/gitlab-com/gl-infra/production/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Incident%3A%3AActive) that are ~severity::1 or ~severity::2
  - If the change involves doing maintenance on a database host, an appropriate silence targeting the host(s) should be added for the duration of the change.