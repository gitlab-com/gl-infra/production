<!--
Please review https://about.gitlab.com/handbook/engineering/infrastructure/change-management/ for the most recent information on our change plans and execution policies.

NOTE: This is an ongoing change template which will be used up to ten times per year by the Database Operations team

NOTE: This change request should be performed for Production OR Staging, SEPERATELY and not both at the same time. Commands for both environments are supplied but I strongly reccomend deleting the commands you do not need before creating an issue from this template.
-->

# Production Change

### Change Summary

We are patching all Gitlab.com PostgreSQL databases to {+ postgres minor version +} minor version. We identified critical and relevant fixes in the latest patches:
* Fix of critical issues:
{+ Fill in critical issues fixed +}
* Fix of non-critical but relevant issues:
{+ Fill in non-critical but relevant issues fixed +}

<!--
You only need to link to staging CR if this is a change to production.
-->

CR in Staging is: {+ Link to CR in Staging +}

#### Downtime Component

We do not expect any downtime component from this upgrade. 

Our "Leader Switchover" should not impact negatively the workload, the switchover processes pause incoming Write requests for a few seconds when switching into a new cluster leader;

There may be a small decrease in AppDex and a small increase in errors, however previous minor release upgrades of postgres did not cause breaches of SLIs.


### Change Details

1. **Services Impacted**      - ~"Service::Patroni" ~"Service::PatroniCI" ~"Service::PatroniRegistry" ~"Service::PatroniSecurity" 
1. **Change Technician**      - <!-- woodhouse: '`@{{ .Username }}`' -->{+ DRI for the execution of this change +}
1. **Change Reviewer**        - <!-- woodhouse: '@{{ .Reviewer }}' -->{+ DRI for the review of this change +}
1. **Time tracking**          - <!-- woodhouse: '{{ .Duration}}' -->{+ Time, in minutes, hours, or days, needed to execute all change steps, including rollback +}
1. **Scheduled Date and Time (UTC in format YYYY-MM-DD HH:MM)** - {+ Start date and time planned to execute change steps YYYY-MM-DD HH:MM +}
1. **Downtime Component**     - We do not expect any downtime component from this upgrade. 


## Detailed steps for the change

<!--
The communication plan below is not required if this is a change to staging
-->

### Communication plan

#### T - 4 Weeks

- [ ] (For production machines only) Communicate with CMOC about the scheduled maintenance in 4 weeks. Open a communication request with the template https://gitlab.com/gitlab-com/gl-infra/production/-/issues/new?issuable_template=external_communication. Post the following message in #whats-happening-at-gitlab, #production, #engineering-fyi, #customer-success. The message in #production should tag @cmoc.
    
    Message:
    ```
    In 4 weeks, we will be undergoing a scheduled maintenance to patch all our database layer to PostgreSQL version {+ postgres minor version +}. The maintenance will start at *Estimated Time to Complete (mins)* - {+Estimated Time to Complete in Minutes+}. We'll keep GitLab.com available during the whole period. We apologise in advance for any inconvenience this may cause. See {+  issue link +}
    ```

#### T - 2 Weeks

- [ ] (For production machines only) Communicate with CMOC about the scheduled maintenance in 2 weeks. Post this message in #whats-happening-at-gitlab, #production, #engineering-fyi, #customer-success. The message in #production should tag @cmoc
    
    Message:
    ```
    In 2 weeks, we will be undergoing a scheduled maintenance to patch all our database layer to PostgreSQL version {+ postgres minor version +}. The maintenance will start at *Estimated Time to Complete (mins)* - {+Estimated Time to Complete in Minutes+}. We'll keep GitLab.com available during the whole period. We apologise in advance for any inconvenience this may cause. See {+  issue link +}
    ```

#### T - 1 Week

- [ ] (For production machines only) Communicate with CMOC about the scheduled maintenance in 1 week. Post this message in #whats-happening-at-gitlab, #production, #engineering-fyi, #customer-success. The message in #production should tag @cmoc
    
    Message:
    ```
    In 1 week, we will be undergoing a scheduled maintenance to patch all our database layer to PostgreSQL version {+ postgres minor version +}. The maintenance will start at *Estimated Time to Complete (mins)* - {+Estimated Time to Complete in Minutes+}. We'll keep GitLab.com available during the whole period. We apologise in advance for any inconvenience this may cause. See {+  issue link +}
    ```

#### T - 3 Days

- [ ] (For production machines only) Communicate with CMOC about the scheduled maintenance in 3 days. Post this message in #production. The message in #production should tag @cmoc
    
    Message:
    ```
    We will be undergoing scheduled maintenance of our databases in 3 days. We'll keep GitLab.com available during the whole period. The maintenance will start at *Estimated Time to Complete (mins)* - {+Estimated Time to Complete in Minutes+}. See {+  issue link +}
    ```

#### T - 1 Day

- [ ] (For production machines only) Communicate with CMOC about the scheduled maintenance in 1 day. Post this message in #production. The message in #production should tag @cmoc
    
    Message:
    ```
    We will be undergoing scheduled maintenance of our databases tomorrow. TWe'll keep GitLab.com available during the whole period. The maintenance will start at *Estimated Time to Complete (mins)* - {+Estimated Time to Complete in Minutes+}. See {+  issue link +}
    ```


### Pre-Change Steps - steps to be completed before execution of the change

#### T - 6 Hours

- [ ] Notify @release-managers in #production

    Message:
    ```
    Hi @release-managers

    We are reaching out to inform that in a 6 Hours we'll start patching all our database clusters in GSTG or GPRD (DELETE ONE). For that we'll be blocking deployments to avoid DDLs (database migrations) during the period. The procedure should take a few hours. We'll inform once we resumed DDL background workers.
    ```

- [ ] Temporarily disable Rails backend workers that perform DDLs by typing the following into #production
    - Staging
    ```slack
    /chatops run feature set disallow_database_ddl_feature_flags true --staging --ignore-production-check --ignore-feature-flag-consistency-check
    ```
    - Production
    ```slack
    /chatops run feature set disallow_database_ddl_feature_flags true --ignore-production-check --ignore-feature-flag-consistency-check
    ```S

#### T - 20 Minutes 

*Estimated Time to Complete (mins)* - 15 min

- [ ] Login to the console server 
    - Staging: (console-01-sv-gstg.c.gitlab-staging-1.internal)
    - Production: (console-01-sv-gprd.c.gitlab-production.internal)

- [ ] Switch to `dbupgrade` user and open a `tmux` session

- [ ] Install Ansible on the Console server:
    ```sh
    rm -rf ~/src/db-migration \
    && cd ~/src \
    && git clone https://gitlab.com/gitlab-com/gl-infra/db-migration.git \
    && cd db-migration \
    && git checkout master

    python3 -m venv ansible
    source ansible/bin/activate
    python3 -m pip install --upgrade pip
    python3 -m pip install ansible
    python3 -m pip install jmespath
    ansible --version
    ```

- [ ] Check SSH access to the Staging or Production boxes from the console server
    - Staging:
        (console-01-sv-gstg.c.gitlab-staging-1.internal)
            ```sh
            cd /home/dbupgrade/src/db-migration/pg-upgrade-minor
            ./bin/ansible-exec.sh -e gstg -p ping4 weeks
            ```
    - Production:
        (console-01-sv-gprd.c.gitlab-production.internal)
            ```sh
            cd /home/dbupgrade/src/db-migration/pg-upgrade-minor
            ./bin/ansible-exec.sh -e gprd -p ping
            ```

### Change Steps - steps to take to execute the change

*Estimated Time to Complete (mins)* - {+Estimated Time to Complete in Minutes+}

- [ ] Set label ~"change::in-progress" `/label ~change::in-progress`

- [ ] (For production machines only) Post in #production tagging @cmoc that we'll start the maintenance/CR. Post update from Status.io maintenance site, publish on @gitlabstatus.
    
    Message:
    ```
    Gitlab.com scheduled maintenance of our database layer have started. GitLab.com should be available during the whole period.  See {+ issue link +}
    ```

- [ ] (For production machines only) Set maintenance mode on the machines so that anything which happens during the upgrade is not counted in the SLAs
    ```
    cd /opt/prometheus/node_exporter/metrics
    touch gitlab_maintenance_mode.prom
    ### Edit gitlab_maintenance_mode.prom:
    # HELP gitlab_maintenance_mode record maintenance window
    # TYPE gitlab_maintenance_mode untyped
    gitlab_maintenance_mode 1
    ```
 
- [ ] (For production machines only) Copy the CR procedure into the Ops so if the downtime is longer than anticipated, we still have a copy of the procedure

- [ ] Execute playbook pre-check on each Cluster:
    - Staging
    ```sh
    cd /home/dbupgrade/src/db-migration/pg-upgrade-minor
    ./bin/ansible-exec.sh -e gstg -h patroni-main-v16-03-db-gstg.c.gitlab-staging-1.internal -t pre-checks
    ./bin/ansible-exec.sh -e gstg -h patroni-ci-v16-03-db-gstg.c.gitlab-staging-1.internal -t pre-checks
    ./bin/ansible-exec.sh -e gstg -h patroni-registry-v16-03-db-gstg.c.gitlab-staging-1.internal -t pre-checks
    ./bin/ansible-exec.sh -e gstg -h patroni-sec-v16-03-db-gstg.c.gitlab-staging-1.internal -t pre-checks
    ```
    - Production
    ```sh
    cd /home/dbupgrade/src/db-migration/pg-upgrade-minor
    ./bin/ansible-exec.sh -e gprd -h patroni-main-v16-103-db-gprd.c.gitlab-production.internal -t pre-checks
    ./bin/ansible-exec.sh -e gprd -h patroni-ci-v16-03-db-gprd.c.gitlab-production.internal -t pre-checks
    ./bin/ansible-exec.sh -e gprd -h patroni-registry-v16-03-db-gprd.c.gitlab-production.internal -t pre-checks
    ./bin/ansible-exec.sh -e gprd -h patroni-sec-v16-03-db-gprd.c.gitlab-production.internal -t pre-checks
    ```

- [ ] Execute `disable_chef` playbook on all nodes:
    - Staging
    ```sh
    cd /home/dbupgrade/src/db-migration/pg-upgrade-minor
    ./bin/ansible-exec.sh -e gstg -p disable_chef
    ```
    - Production
    ```sh
    cd /home/dbupgrade/src/db-migration/pg-upgrade-minor
    ./bin/ansible-exec.sh -e gprd -p disable_chef
    ```

- [ ] Execute the playbook for all Patroni Replica nodes on `gstg` or `gprd` environment
    - Staging
    ```shS
    cd /home/dbupgrade/src/db-migration/pg-upgrade-minor
    ./bin/ansible-exec.sh -e gstg
    ```
    - Production
    ```sh
    cd /home/dbupgrade/src/db-migration/pg-upgrade-minor
    ./bin/ansible-exec.sh -e gprd
    ```

- [ ] Wait all Replicas to get back in sync and for the workload to get balanced;

- [ ] (For production machines only) Check with EOC if there are no S1/S2 incidents

- [ ] Take note of the current Leader hostname of each cluster and paste this information into the issue comments.
    - Staging
    ```sh
    ssh patroni-registry-v16-01-db-gstg.c.gitlab-staging-1.internal "sudo gitlab-patronictl list"
    ssh patroni-sec-v16-01-db-gstg.c.gitlab-staging-1.internal "sudo gitlab-patronictl list"
    ssh patroni-ci-v16-01-db-gstg.c.gitlab-staging-1.internal "sudo gitlab-patronictl list"
    ssh patroni-main-v16-01-db-gstg.c.gitlab-staging-1.internal "sudo gitlab-patronictl list"
    ```
    - Production
    ```sh
    ssh patroni-registry-v16-01-db-gprd.c.gitlab-production.internal "sudo gitlab-patronictl list"
    ssh patroni-sec-v16-01-db-gprd.c.gitlab-production.internal "sudo gitlab-patronictl list"
    ssh patroni-ci-v16-01-db-gprd.c.gitlab-production.internal "sudo gitlab-patronictl list"
    ssh patroni-main-v16-101-db-gprd.c.gitlab-production.internal "sudo gitlab-patronictl list"
    ```

- [ ] Execute a switchover of each Cluster
    - Staging

        - [ ] Cluster `patroni-registry-v16`
            ```sh
            cd /home/dbupgrade/src/db-migration/dbre-toolkit
            ansible-playbook -i inventory/gstg-registry.yml switchover_patroni_leader.yml 2>&1 | ts | tee -a ansible_switchover_patroni_leader_gstg-registry_$(date +%Y%m%d).log
            ```

        - [ ] Cluster `patroni-sec-v16`
            ```sh
            cd /home/dbupgrade/src/db-migration/dbre-toolkit
            ansible-playbook -i inventory/gstg-sec.yml switchover_patroni_leader.yml 2>&1 | ts | tee -a ansible_switchover_patroni_leader_gstg-sec_$(date +%Y%m%d).log
            ```

        - [ ] Cluster `patroni-ci-v16`
            ```sh
            cd /home/dbupgrade/src/db-migration/dbre-toolkit
            ansible-playbook -i inventory/gstg-ci.yml switchover_patroni_leader.yml 2>&1 | ts | tee -a ansible_switchover_patroni_leader_gstg-ci_$(date +%Y%m%d).log
            ```

        - [ ] Cluster `patroni-v16`
            ```sh
            cd /home/dbupgrade/src/db-migration/dbre-toolkit
            ansible-playbook -i inventory/gstg-main.yml switchover_patroni_leader.yml 2>&1 | ts | tee -a ansible_switchover_patroni_leader_gstg-main_$(date +%Y%m%d).log
            ```

    - Production
        - [ ] Cluster `patroni-registry-v16`
            ```sh
            cd /home/dbupgrade/src/db-migration/dbre-toolkit
            ansible-playbook -i inventory/gprd-registry.yml switchover_patroni_leader.yml 2>&1 | ts | tee -a ansible_switchover_patroni_leader_gprd-registry_$(date +%Y%m%d).log
            ```

        - [ ] Cluster `patroni-sec-v16`
            ```sh
            cd /home/dbupgrade/src/db-migration/dbre-toolkit
            ansible-playbook -i inventory/gprd-sec.yml switchover_patroni_leader.yml 2>&1 | ts | tee -a ansible_switchover_patroni_leader_gprd-sec_$(date +%Y%m%d).log
            ```

        - [ ] Cluster `patroni-ci-v16`
            ```sh
            cd /home/dbupgrade/src/db-migration/dbre-toolkit
            ansible-playbook -i inventory/gprd-ci.yml switchover_patroni_leader.yml 2>&1 | ts | tee -a ansible_switchover_patroni_leader_gprd-ci_$(date +%Y%m%d).log
            ```

        - [ ] Cluster `patroni-v16`
            ```sh
            cd /home/dbupgrade/src/db-migration/dbre-toolkit
            ansible-playbook -i inventory/gprd-main.yml switchover_patroni_leader.yml 2>&1 | ts | tee -a ansible_switchover_patroni_leader_gprd-main_$(date +%Y%m%d).log
            ```

- [ ] Re-enable Rails backend workers that perform DDLs by typing the following into #production
    - Staging
    ```slack
    /chatops run feature set disallow_database_ddl_feature_flags false --staging --ignore-production-check --ignore-feature-flag-consistency-check
    ```
    - Production
    ```slack
    /chatops run feature set disallow_database_ddl_feature_flags false --ignore-production-check --ignore-feature-flag-consistency-check
    ```

- [ ] Notify @release-managers in #production
    Message:
    ```
    Hi @release-managers
    We finished the Switchover procedure, hence the DDL background workers are enabled again and all database migrations queues will be processed.
    ```

- [ ] Execute the playbook for all former LEADER nodes on `gstg` OR `gprd` environment, one at a time:
    - Staging
        - [ ] Registry: `export UPGD_NODE="patroni-registry-v16-01-db-gstg.c.gitlab-staging-1.internal"`
        - [ ] SEC: `export UPGD_NODE="patroni-sec-v16-01-db-gstg.c.gitlab-staging-1.internal"`
        - [ ] CI: `export UPGD_NODE="patroni-ci-v16-01-db-gstg.c.gitlab-staging-1.internal"`
        - [ ] MAIN: `export UPGD_NODE="patroni-main-v16-01-db-gstg.c.gitlab-staging-1.internal"`

    - Production
        - [ ] Registry: `export UPGD_NODE="patroni-registry-v16-01-db-gprd.c.gitlab-production.internal"`
        - [ ] SEC: `export UPGD_NODE="patroni-sec-v16-01-db-gprd.c.gitlab-production.internal"`
        - [ ] CI: `export UPGD_NODE="patroni-ci-v16-01-db-gprd.c.gitlab-production.internal"`
        - [ ] MAIN: `export UPGD_NODE="patroni-main-v16-101-db-gprd.c.gitlab-production.internal"`

    Replace UPGD_NODE with a node from the list above.
    - Staging
    ```sh
    cd /home/dbupgrade/src/db-migration/pg-upgrade-minor
    ./bin/ansible-exec.sh -e gstg -h $UPGD_NODE
    ```
    - Production
    ```sh
    cd /home/dbupgrade/src/db-migration/pg-upgrade-minor
    ./bin/ansible-exec.sh -e gprd -h $UPGD_NODE
    ```

- [ ] Upgrade the PostgreSQL Extensions on all clusters
    - Staging
    ```sh
    cd /home/dbupgrade/src/db-migration/pg-upgrade-minor
    ./bin/ansible-exec.sh -e gstg -p upgrade_psql_extensions -x "update_all_extensions=true"
    ```
    - Production
    ```sh
    cd /home/dbupgrade/src/db-migration/pg-upgrade-minor
    ./bin/ansible-exec.sh -e gprd -p upgrade_psql_extensions -x "update_all_extensions=true"
    ```

- [ ] Connect to the Postgresql and verify the version:
    - Staging
    ```sh
    knife ssh "roles:gstg-base-db-patroni-main-v16" "gitlab-psql -At -c \"select * from version();\"" | sort
    knife ssh "roles:gstg-base-db-patroni-ci-v16" "gitlab-psql -At -c \"select * from version();\"" | sort
    knife ssh "roles:gstg-base-db-patroni-sec-v16" "gitlab-psql -At -c \"select * from version();\"" | sort
    knife ssh "roles:gstg-base-db-patroni-registry-v16" "gitlab-psql -At -c \"select * from version();\"" | sort
    ```
    - Production
    ```sh
    knife ssh "roles:gprd-base-db-patroni-main-v16 OR roles:gprd-base-db-patroni-ci-v16 OR roles:gprd-base-db-patroni-sec-v16 OR roles:gprd-base-db-patroni-registry-v16" "gitlab-psql -At -c \"select * from version();\"" | sort
    ```

- [ ] Execute Chef client: 
    - Staging
    ```sh
    knife ssh "roles:gstg-base-db-patroni-main-v16" "sudo chef-client-enable; sudo chef-client;"
    knife ssh "roles:gstg-base-db-patroni-ci-v16" "sudo chef-client-enable; sudo chef-client;"
    knife ssh "roles:gstg-base-db-patroni-sec-v16" "sudo chef-client-enable; sudo chef-client;"
    knife ssh "roles:gstg-base-db-patroni-registry-v16" "sudo chef-client-enable; sudo chef-client;"
    ```
    - Production
    ```sh
    knife ssh "roles:gprd-base-db-patroni-main-v16 OR roles:gprd-base-db-patroni-ci-v16 OR roles:gprd-base-db-patroni-sec-v16 OR roles:gprd-base-db-patroni-registry-v16" "sudo chef-client-enable; sudo chef-client;"
    ```

- [ ] Run the QA smoke tests

- [ ] (For production machines only) Revert the maintenance mode on the patroni instances so that alerting would be counted towards SLOs again
    ```
    ### Edit gitlab_maintenance_mode.prom:
    # HELP gitlab_maintenance_mode record maintenance window
    # TYPE gitlab_maintenance_mode untyped
    gitlab_maintenance_mode 0
    ```

- [ ] (For production machines only) Communicate in #production using @cmoc that the maintenance/CR has finished. Post update from Status.io maintenance site, publish on @gitlabstatus.

    Message:
    ```
    Gitlab.com database maintenance was performed, the platform is considered stable. We'll continue to monitor for any performance issues. Thank you for your patience. See {+  issue link +}
    ```

- [ ] Set label ~"change::complete" `/label ~change::complete`
S

## Rollback

### Rollback steps - steps to be taken in the event of a need to rollback this change

*Estimated Time to Complete (mins)* - {+Estimated Time to Complete in Minutes+}

- [ ] (For production machines only) Communicate in #production using @cmoc that we'll start rollback of the maintenance. Post update from Status.io maintenance site, publish on @gitlabstatus.
    
    Message:
    ```
    Due to an issue during the planned maintenance for the database layer, we have initiated a rollback. No customer impact is expected. We will provide an update once the rollback process is completed.
    ```

- [ ] Execute `disable_chef` playbook on all nodes:
    - Staging
    ```sh
    cd /home/dbupgrade/src/db-migration/pg-upgrade-minor
    ./bin/ansible-exec.sh -e gstg -p disable_chef
    ```
    - Production
    ```sh
    cd /home/dbupgrade/src/db-migration/pg-upgrade-minor
    ./bin/ansible-exec.sh -e gprd -p disable_chef
    ```

- [ ] Execute the `rollback` for all Patroni Replica nodes on `gstg` or `gprd` environment
    - Staging
    ```sh
    cd /home/dbupgrade/src/db-migration/pg-upgrade-minor
    ./bin/ansible-exec.sh -e gstg -p rollback
    ```
    - Production
    ```sh
    cd /home/dbupgrade/src/db-migration/pg-upgrade-minor
    ./bin/ansible-exec.sh -e gprd -p rollback
    ```

- [ ] Wait all Replicas to get back in sync and for the workload to get balanced;

- [ ] Take note of the current Leader hostname of each cluster and paste this information into the issue comments.
    - Staging
    ```sh
    ssh patroni-registry-v16-01-db-gstg.c.gitlab-staging-1.internal "sudo gitlab-patronictl list"
    ssh patroni-sec-v16-01-db-gstg.c.gitlab-staging-1.internal "sudo gitlab-patronictl list"
    ssh patroni-ci-v16-01-db-gstg.c.gitlab-staging-1.internal "sudo gitlab-patronictl list"
    ssh patroni-main-v16-01-db-gstg.c.gitlab-staging-1.internal "sudo gitlab-patronictl list"
    ```
    - Production
    ```sh
    ssh patroni-registry-v16-01-db-gprd.c.gitlab-production.internal "sudo gitlab-patronictl list"
    ssh patroni-sec-v16-01-db-gprd.c.gitlab-production.internal "sudo gitlab-patronictl list"
    ssh patroni-ci-v16-01-db-gprd.c.gitlab-production.internal "sudo gitlab-patronictl list"
    ssh patroni-main-v16-101-db-gprd.c.gitlab-production.internal "sudo gitlab-patronictl list"
    ```

- [ ] Execute a switchover of each Cluster
    - Staging
        - [ ] Cluster `patroni-registry-v16`
            ```sh
            cd /home/dbupgrade/src/db-migration/dbre-toolkit
            ansible-playbook -i inventory/gstg-registry.yml switchover_patroni_leader.yml 2>&1 | ts | tee -a ansible_switchover_patroni_leader_gstg-registry_$(date +%Y%m%d).log
            ```

        - [ ] Cluster `patroni-sec-v16`
            ```sh
            cd /home/dbupgrade/src/db-migration/dbre-toolkit
            ansible-playbook -i inventory/gstg-sec.yml switchover_patroni_leader.yml 2>&1 | ts | tee -a ansible_switchover_patroni_leader_gstg-sec_$(date +%Y%m%d).log
            ```

        - [ ] Cluster `patroni-ci-v16`
            ```sh
            cd /home/dbupgrade/src/db-migration/dbre-toolkit
            ansible-playbook -i inventory/gstg-ci.yml switchover_patroni_leader.yml 2>&1 | ts | tee -a ansible_switchover_patroni_leader_gstg-ci_$(date +%Y%m%d).log
            ```

        - [ ] Cluster `patroni-v16`
            ```sh
            cd /home/dbupgrade/src/db-migration/dbre-toolkit
            ansible-playbook -i inventory/gstg-main.yml switchover_patroni_leader.yml 2>&1 | ts | tee -a ansible_switchover_patroni_leader_gstg-main_$(date +%Y%m%d).log
            ```

    - Production
        - [ ] Cluster `patroni-registry-v16`
            ```sh
            cd /home/dbupgrade/src/db-migration/dbre-toolkit
            ansible-playbook -i inventory/gprd-registry.yml switchover_patroni_leader.yml 2>&1 | ts | tee -a ansible_switchover_patroni_leader_gprd-registry_$(date +%Y%m%d).log
            ```

        - [ ] Cluster `patroni-sec-v16`
            ```sh
            cd /home/dbupgrade/src/db-migration/dbre-toolkit
            ansible-playbook -i inventory/gprd-sec.yml switchover_patroni_leader.yml 2>&1 | ts | tee -a ansible_switchover_patroni_leader_gprd-sec_$(date +%Y%m%d).log
            ```

        - [ ] Cluster `patroni-ci-v16`
            ```sh
            cd /home/dbupgrade/src/db-migration/dbre-toolkit
            ansible-playbook -i inventory/gprd-ci.yml switchover_patroni_leader.yml 2>&1 | ts | tee -a ansible_switchover_patroni_leader_gprd-ci_$(date +%Y%m%d).log
            ```

        - [ ] Cluster `patroni-v16`
            ```sh
            cd /home/dbupgrade/src/db-migration/dbre-toolkit
            ansible-playbook -i inventory/gprd-main.yml switchover_patroni_leader.yml 2>&1 | ts | tee -a ansible_switchover_patroni_leader_gprd-main_$(date +%Y%m%d).log
            ```

- [ ] Re-enable Rails backend workers that perform DDLs by typing the following into #production
    - Staging
    ```slack
    /chatops run feature set disallow_database_ddl_feature_flags false --staging --ignore-production-check --ignore-feature-flag-consistency-check
    ```

    - Production
    ```slack
    /chatops run feature set disallow_database_ddl_feature_flags false --ignore-production-check --ignore-feature-flag-consistency-check
    ```

- [ ] Notify @release-managers in #g_delivery
    Message:
    ```
    Hi @release-managers
    We rolled back the patch process, hence the DDL background workers are enabled again and all database migrations queues will be processed.
    ```

- [ ] Execute the playbook for all former LEADER nodes on `gstg` or `gprd` environment, one at a time:
    - Staging
        - [ ] Registry: `export UPGD_NODE="patroni-registry-v16-02-db-gstg.c.gitlab-staging-1.internal"`
        - [ ] SEC: `export UPGD_NODE="patroni-sec-v16-02-db-gstg.c.gitlab-staging-1.internal"`
        - [ ] CI: `export UPGD_NODE="patroni-ci-v16-02-db-gstg.c.gitlab-staging-1.internal"`
        - [ ] MAIN: `export UPGD_NODE="patroni-main-v16-02-db-gstg.c.gitlab-staging-1.internal"`
        
    - Production
        - [ ] Registry: `export UPGD_NODE="patroni-registry-v16-02-db-gprd.c.gitlab-production.internal"`
        - [ ] SEC: `export UPGD_NODE="patroni-sec-v16-02-db-gprd.c.gitlab-production.internal"`
        - [ ] CI: `export UPGD_NODE="patroni-ci-v16-02-db-gprd.c.gitlab-production.internal"`
        - [ ] MAIN: `export UPGD_NODE="patroni-main-v16-102-db-gprd.c.gitlab-production.internal"`

    Replace UPGD_NODE with a node from the list above.
    - Staging
    ```sh
    cd /home/dbupgrade/src/db-migration/pg-upgrade-minor
    ./bin/ansible-exec.sh -e gstg -p rollback -h $UPGD_NODE
    ```
    - Production
    ```sh
    cd /home/dbupgrade/src/db-migration/pg-upgrade-minor
    ./bin/ansible-exec.sh -e gprd -p rollback -h $UPGD_NODE
    ```

- [ ] Connect to the Postgresql and verify the version: 
    - Staging
    ```sh
    knife ssh "roles:gstg-base-db-patroni-main-v16" "gitlab-psql -At -c \"select * from version();\"" | sort
    knife ssh "roles:gstg-base-db-patroni-ci-v16" "gitlab-psql -At -c \"select * from version();\"" | sort
    knife ssh "roles:gstg-base-db-patroni-sec-v16" "gitlab-psql -At -c \"select * from version();\"" | sort
    knife ssh "roles:gstg-base-db-patroni-registry-v16" "gitlab-psql -At -c \"select * from version();\"" | sort
    ```
    - Production
    ```sh
    knife ssh "roles:gprd-base-db-patroni-main-v16 OR roles:gprd-base-db-patroni-ci-v16 OR roles:gprd-base-db-patroni-sec-v16 OR roles:gprd-base-db-patroni-registry-v16" "gitlab-psql -At -c \"select * from version();\"" | sort
    ```

- [ ] Execute Chef client: 
    - Staging
    ```sh
    knife ssh "roles:gstg-base-db-patroni-main-v16" "sudo chef-client-enable; sudo chef-client;"
    knife ssh "roles:gstg-base-db-patroni-ci-v16" "sudo chef-client-enable; sudo chef-client;"
    knife ssh "roles:gstg-base-db-patroni-sec-v16" "sudo chef-client-enable; sudo chef-client;"
    knife ssh "roles:gstg-base-db-patroni-registry-v16" "sudo chef-client-enable; sudo chef-client;"
    ```
    - Production
    ```sh
    knife ssh "roles:gprd-base-db-patroni-main-v16 OR roles:gprd-base-db-patroni-ci-v16 OR roles:gprd-base-db-patroni-sec-v16 OR roles:gprd-base-db-patroni-registry-v16" "sudo chef-client-enable; sudo chef-client;"
    ```

- [ ] Run the QA smoke tests

- [ ] (For production machines only) Revert the maintenance mode on the patroni instances so that alerting would be counted towards SLOs again
    ```
    ### Edit gitlab_maintenance_mode.prom:
    # HELP gitlab_maintenance_mode record maintenance window
    # TYPE gitlab_maintenance_mode untyped
    gitlab_maintenance_mode 0
    ```

- [ ] (For production machines only) Communicate in #production using @cmoc that the maintenance/CR has finished with failure and that we had to rollback the process. Post update from Status.io maintenance site, publish on @gitlabstatus.

    Message:
    ```
    GitLab.com rollback for the database layer is complete, and we're back up and running. We'll be monitoring the platform to ensure all systems are functioning correctly. Thank you for your patience.
    ```

- [ ] Set label ~"change::aborted" `/label ~change::aborted`

## Monitoring

### Key metrics to observe

<!--
  * Describe which dashboards and which specific metrics we should be monitoring related to this change using the format below.
-->

- Staging

    * Metric: Patroni MAIN Overview dashboard
    * Location: https://dashboards.gitlab.net/d/patroni-main/patroni3a-overview?orgId=1&from=now-3h&to=now&timezone=utc&var-PROMETHEUS_DS=mimir-gitlab-gstg&var-environment=gstg
    * What changes to this metric should prompt a rollback: Any deviation from the normal state, especially sustained Error Ratio or Resource Saturations (CPU Utilization, Disk I/O, Memory usage, etc.).

    * Metric: Patroni CI Overview dashboard
    * Location: https://dashboards.gitlab.net/d/patroni-ci-main/patroni-ci3a-overview?orgId=1&from=now-3h&to=now&timezone=utc&var-PROMETHEUS_DS=mimir-gitlab-gstg&var-environment=gstg
    * What changes to this metric should prompt a rollback: Any deviation from the normal state, especially sustained Error Ratio or Resource Saturations (CPU Utilization, Disk I/O, Memory usage, etc.).

    * Metric: Patroni REGISTRY Overview dashboard
    * Location: https://dashboards.gitlab.net/d/patroni-registry-main/patroni-registry3a-overview?orgId=1&from=now-3h&to=now&timezone=utc&var-PROMETHEUS_DS=mimir-gitlab-gstg&var-environment=gstg
    * What changes to this metric should prompt a rollback: Any deviation from the normal state, especially sustained Error Ratio or Resource Saturations (CPU Utilization, Disk I/O, Memory usage, etc.).

    * Metric: Patroni SEC Overview dashboard
    * Location: https://dashboards.gitlab.net/d/patroni-sec-main/patroni-sec3a-overview?orgId=1&from=now-6h%2Fm&to=now%2Fm&timezone=utc&var-PROMETHEUS_DS=mimir-gitlab-gstg&var-environment=gstg
    * What changes to this metric should prompt a rollback: Any deviation from the normal state, especially sustained Error Ratio or Resource Saturations (CPU Utilization, Disk I/O, Memory usage, etc.).

    * Metric: Web Service WEB Overview dashboard
    * Location: https://dashboards.gitlab.net/d/web-main/web3a-overview?from=now-6h%2Fm&orgId=1&timezone=utc&to=now%2Fm&var-PROMETHEUS_DS=mimir-gitlab-gstg&var-environment=gstg&var-stage=main
    * What changes to this metric should prompt a rollback: Any deviation from the normal state, especially sustained Error Ratio or Apdex degradation.

    * Metric: Sidekiq Service SIDEKIQ Overview dashboard
    * Location: https://dashboards.gitlab.net/d/sidekiq-main/sidekiq3a-overview?orgId=1&from=now-6h%2Fm&to=now%2Fm&timezone=utc&var-PROMETHEUS_DS=mimir-gitlab-gstg&var-environment=gstg&var-stage=main&var-shard=catchall
    * What changes to this metric should prompt a rollback: Any deviation from the normal state, especially sustained Error Ratio or Apdex degradation.

- Production

    * Metric: Patroni MAIN Overview dashboard
    * Location: https://dashboards.gitlab.net/d/patroni-main/patroni3a-overview?orgId=1&from=now-3h&to=now&timezone=utc&var-PROMETHEUS_DS=mimir-gitlab-gprd&var-environment=gprd
    * What changes to this metric should prompt a rollback: Any deviation from the normal state, especially sustained Error Ratio or Resource Saturations (CPU Utilization, Disk I/O, Memory usage, etc.).

    * Metric: Patroni CI Overview dashboard
    * Location: https://dashboards.gitlab.net/d/patroni-ci-main/patroni-ci3a-overview?orgId=1&from=now-3h&to=now&timezone=utc&var-PROMETHEUS_DS=mimir-gitlab-gprd&var-environment=gprd
    * What changes to this metric should prompt a rollback: Any deviation from the normal state, especially sustained Error Ratio or Resource Saturations (CPU Utilization, Disk I/O, Memory usage, etc.).

    * Metric: Patroni REGISTRY Overview dashboard
    * Location: https://dashboards.gitlab.net/d/patroni-registry-main/patroni-registry3a-overview?orgId=1&from=now-3h&to=now&timezone=utc&var-PROMETHEUS_DS=mimir-gitlab-gprd&var-environment=gprd
    * What changes to this metric should prompt a rollback: Any deviation from the normal state, especially sustained Error Ratio or Resource Saturations (CPU Utilization, Disk I/O, Memory usage, etc.).

    * Metric: Patroni SEC Overview dashboard
    * Location: https://dashboards.gitlab.net/d/patroni-sec-main/patroni-sec3a-overview?orgId=1&from=now-6h%2Fm&to=now%2Fm&timezone=utc&var-PROMETHEUS_DS=mimir-gitlab-gprd&var-environment=gprd
    * What changes to this metric should prompt a rollback: Any deviation from the normal state, especially sustained Error Ratio or Resource Saturations (CPU Utilization, Disk I/O, Memory usage, etc.).

    * Metric: Web Service WEB Overview dashboard
    * Location: https://dashboards.gitlab.net/d/web-main/web3a-overview?from=now-6h%2Fm&orgId=1&timezone=utc&to=now%2Fm&var-PROMETHEUS_DS=mimir-gitlab-gprd&var-environment=gprd&var-stage=main
    * What changes to this metric should prompt a rollback: Any deviation from the normal state, especially sustained Error Ratio or Apdex degradation.

    * Metric: Sidekiq Service SIDEKIQ Overview dashboard
    * Location: https://dashboards.gitlab.net/d/sidekiq-main/sidekiq3a-overview?orgId=1&from=now-6h%2Fm&to=now%2Fm&timezone=utc&var-PROMETHEUS_DS=mimir-gitlab-gprd&var-environment=gprd&var-stage=main&var-shard=catchall
    * What changes to this metric should prompt a rollback: Any deviation from the normal state, especially sustained Error Ratio or Apdex degradation.


## Change Reviewer checklists

<!--
To be filled out by the reviewer.
-->
~C4 ~C3 ~C2 ~C1:

- [ ] Check if the following applies:
  - The **scheduled day and time** of execution of the change is appropriate.
  - The [change plan](#detailed-steps-for-the-change) is technically accurate.
  - The change plan includes **estimated timing values** based on previous testing.
  - The change plan includes a viable [rollback plan](#rollback).
  - The specified [metrics/monitoring dashboards](#key-metrics-to-observe) provide sufficient visibility for the change.

~C2 ~C1:

- [ ] Check if the following applies:
  - The complexity of the plan is appropriate for the corresponding risk of the change. (i.e. the plan contains clear details).
  - The change plan includes success measures for all steps/milestones during the execution.
  - The change adequately minimizes risk within the environment/service.
  - The performance implications of executing the change are well-understood and documented.
  - The specified metrics/monitoring dashboards provide sufficient visibility for the change.
    - If not, is it possible (or necessary) to make changes to observability platforms for added visibility?
  - The change has a primary and secondary SRE with knowledge of the details available during the change window.
  - The change window has been agreed with Release Managers in advance of the change. If the change is planned for APAC hours, this issue has an agreed pre-change approval.
  - The labels ~"blocks deployments" and/or ~"blocks feature-flags" are applied as necessary.

## Change Technician checklist

<!--
To find out who is on-call, use the `@sre-oncall` handle in slack
-->

- [ ] Check if all items below are complete:
  - The [change plan](#detailed-steps-for-the-change) is technically accurate.
  - This Change Issue is linked to the appropriate Issue and/or Epic
  - Change has been tested in staging and results noted in a comment on this issue.
  - A dry-run has been conducted and results noted in a comment on this issue.
  - The change execution window respects the [Production Change Lock periods](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/#production-change-lock-pcl).
  - For ~C1 and ~C2 change issues, the change event is added to the [GitLab Production](https://calendar.google.com/calendar/embed?src=gitlab.com_si2ach70eb1j65cnu040m3alq0%40group.calendar.google.com) calendar.
  - For ~C1 and ~C2 change issues, the SRE on-call has been informed prior to change being rolled out. (In #production channel, mention `@sre-oncall` and this issue and await their acknowledgement.)
  - For ~C1 and ~C2 change issues, the SRE on-call provided approval with the ~eoc_approved label on the issue.
  - For ~C1 and ~C2 change issues, the Infrastructure Manager provided approval with the ~manager_approved label on the issue. Mention `@gitlab-org/saas-platforms/inframanagers` in this issue to request approval and provide visibility to all infrastructure managers.
  - Release managers have been informed prior to any C1, C2, or ~"blocks deployments" change being rolled out. (In #production channel, mention `@release-managers` and this issue and await their acknowledgment.)
  - There are currently no [active incidents](https://gitlab.com/gitlab-com/gl-infra/production/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[ ]=Incident%3A%3AActive) that are ~severity::1 or ~severity::2
  - If the change involves doing maintenance on a database host, an appropriate silence targeting the host(s) should be added for the duration of the change.

  /labels ~C2 ~"blocks deployments" ~"blocks feature-flags" ~"type::maintenance"