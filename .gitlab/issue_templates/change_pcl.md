/label ~change ~"change::scheduled" ~"type::ignore" ~C1 ~pcl ~"blocks feature-flags"

# Production Change Lock

Checkout https://about.gitlab.com/handbook/engineering/infrastructure/change-management/#production-change-lock-pcl for more information on PCLs.

### Lock Summary

<!--
See [list of previous PCL CRs](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/?sort=updated_desc&state=all&label_name%5B%5D=pcl&first_page_size=100) for inspiration.
-->

### Lock Details

<!--
To automatically add the lock dates to the GitLab Production calendar update the following fields:
- Time tracking
- Scheduled Date and Time (UTC in format YYYY-MM-DD HH:MM)

Bot: https://gitlab.com/gitlab-com/gl-infra/ops-team/toolkit/change-scheduler
-->

1. **Lock type**  - {+ Hard/Soft +}
1. **Lock DRI**  - {+ Engineering Manager DRI for ensuring the lock status is accurate +}
1. **Start Date** - {+ Date and time when the lock starts +}
1. **End Date** - {+ Date and time when the lock ends +}
1. **Scheduled Date and Time (UTC in format YYYY-MM-DD HH:MM)** - {+ Start date and time planned to execute change steps YYYY-MM-DD HH:MM +}
1. **Time tracking**          - <!-- woodhouse: '{{ .Duration}}' -->{+ Time, in minutes, hours, or days, needed to execute all change steps, including rollback +}

## Detailed steps for the lock

### Change Steps - steps to take to execute the change

- [ ] Ensure a DRI is assigned to this issue.
- [ ] Prepare slack announcement, edit as necessary `:info::megaphone: [Production Change Lock](URL to this issue) is now set between {Insert start Date & Time} UTC - {Insert end Date & Time} UTC. During PCL, we halt Production Deployments, Feature Flag as well Infrastructure Config changes, see the [Handbook](https://handbook.gitlab.com/handbook/engineering/infrastructure/change-management/#production-change-lock-pcl) for detailed information.`
- [ ] Set label ~"change::in-progress" `/label ~change::in-progress` on the Start Date.
- [ ] Publish the slack message to `#infrastructure-lounge`, cross link to `#production`, `#infrastructure-department` as well as `#whats-happening-at-gitlab`.
    - [ ] [Optional] pin the message in the `#infrastructure-lounge` and `#production` channels.
- [ ] Set label ~"change::complete" `/label ~change::complete` on the End Date.
