# Production Change - Criticality 1 ~C1

## Change Summary

| Role | Assignee |
| --- | --- |
| Change Technician | <!-- woodhouse: '`@{{ .Username }}`' --> |
| Change Technician II | <!-- woodhouse: '@{{ .Reviewer }}' --> |

- **Services Impacted**  - All services
- **Downtime Component** - <!-- woodhouse: '{{ .Downtime }}'--> 96 hours

This production issue is to be used for Gamedays as well as recovery in case of a regional outage.

### Services in Infrastructure

| Service | Region(s) | Data Recovery |
| --- | --- | --- |
| Gitaly | `us-east1` | Restored from snapshots stored in a multi-region bucket |
| PGBouncer | `us-east1` | N/A |
| Patroni | `us-east1` | Restored from backups are in a multi-region bucket |
| Redis Cluster | `us-east1` |  N/A |
| HAProxy | `us-east1` | N/A |
| GCP Load Balancing | `us-east1` | N/A |
| Cloudflare | multiple | N/A |
| CI Runners | `us-east1` | N/A |
| CustomersDot | `us-east1` | Restored from multi-region backups, managed by CloudSQL |
| gprd-gitlab-gke cluster | `us-east1` | N/A |
| gprd-us-east1-{b,c,d} cluster | `us-east1` | N/A |
| Runway | multiple, see [inventory](https://gitlab.com/gitlab-com/gl-infra/platform/runway/provisioner/-/blob/main/inventory.json) | N/A |

### Operations Services in Infrastructure

| Service | Region | Data Recovery |
| --- | --- | --- |
| Chef Server | `us-central1` | Restored from snapshots stored in a multi-region bucket |
| ops-central GKE cluster | `us-central1` | N/A |
| ops-gitlab-gke cluster | `us-east1` | N/A |
| monitoring | `us-east1` | Data is stored in multi-region buckets |

## Recovery

### Phase 1: Recovery Workstream Assignments and Plan

In this section a DRI will need to be chosen for every impacted service to start the recovery.
By splitting the recovery into multiple workstream operations can happen in parallel to improve the recovery time.

_Fill in the table with DRI assignments depending on the service impact_

| Service | DRI |
| --- | --- |
| <service> | <DRI> |

### Phase 2: Service Recovery Procedure

In this section the service recovery is split into issues that will log the steps to recover.

_Fill in the table with issue links for the services that are being recovered._

| Service | Recovery Issue |
| --- | --- |
| <service> | <issue link>

### Phase 3: Validation and Testing

Following the recovery this phase will be used to validate functionality post recovery.
